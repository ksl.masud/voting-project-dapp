pragma solidity ^0.4.22;
import "./BasicVotingContract.sol";

contract MultipleSelectVoting is BasicVotingContract {
    
    
    mapping (uint => bytes32) optionMap;                 // optionMap is used for mapping index to option string like (1 => "Yes"), it's indexing from 1 to n.
    mapping (uint => uint) resultMap;                    // resultMap stores each option's vote count like (1 => 5)
    uint optionCount;                                    // optionCount stores the total number of options




    /** 
     * @notice Inserts all necessary values, sets base contract's constructor's arguments and initialize optionMap
     * @param _applicationId is for tracking the application.
     * @param _article is byte array which acts as a vote subject
     * @param _startTime in second which uses to track the start time of the voting contract
     * @param _deadLine in second which uses to track the end time of the voting contract
     * @param _konaIds which holds konaIds of each participant
     * @param _ballotCount which holds the number of ballots of each participant
     * @dev Change according to requirement
     */
    constructor (
        uint _applicationId,
        uint _votingContractType,
        string _article,
        uint _startTime,
        uint _deadLine,
        bytes32[] _konaIds,
        uint[] _ballotCount,
        bytes32[] _options,
        address _addressBindingContract
    ) 
        BasicVotingContract (
            _applicationId,
            _votingContractType,
            _article,
            _startTime,
            _deadLine,
            _konaIds,
            _ballotCount,
            _addressBindingContract
        ) 
        public
    {
        optionCount = _options.length + 1;
        for (uint i = 0; i < _options.length; i++) {
            optionMap[i+1] = _options[i];
        }
    }
    
    
    /** 
     * @notice This method returns the name of the option according to index.
     * @param _index - the id of option
     * @dev Change according to requirement
     **/
    function getOption (uint _index)
        public
        view
        returns (bytes32 _option) 
    {
        require(_index > 0 && _index <= optionCount, "Invalid Option Selected");
        return optionMap[_index];
    }
    
    
    
    /** 
     * @notice This method returns the total vote count for the option
     * @param _index which is the id of option
     * @dev Change according to requirement
     **/
    function getVoteForOption (uint _index)
        public
        view
        returns (uint _votes)
    {
        require(_index > 0 && _index <= optionCount, "Invalid Option Selected");
        return resultMap[_index];
    }
    
    
    /** 
     * @notice This method casts vote from specific konaId. This method should be changed.
     * @param _index - which is the array of selections in order of options listed in blockchain
     * @param _konaId which is the id of a participant
     * @dev This method will be changed after reviewing
     **/
    function castVote (
        uint[] _index,
        bytes32 _konaId
    ) 
        public
        verifyKonaID (_konaId)
        beforeDeadLine
        canCast (_konaId)
    {
        require(_index.length == optionCount, "Selected Option Array must have same length as the options");
        updateBallotCount (_konaId);
        for(uint i=0; i< optionCount; i++){
            require(_index[i] == 0 || _index[i] == 1, "Invalid Parameter");
            if(_index[i] == 1)
                resultMap[i] = resultMap[i] +  participants[_konaId].ballotCount;
        
        }
    }
    
     /** 
     * @notice This method returns the total count of options
     * @param _optionCount which is the total count of options
     * @dev Change according to requirement
     **/
     
    function getNumberofOption() public view returns(uint _optionCount){
        return optionCount;
    }
    
    
}