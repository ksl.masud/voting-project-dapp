pragma solidity ^0.4.22;
import "./BasicVotingContract.sol";

contract ScoreVoting is BasicVotingContract {
    
    
    mapping (uint => uint) resultMap;                    // resultMap stores each option's vote count
    uint[] optionList;                                   // List of options selected so far
    /** 
     * @notice Inserts all necessary values, sets base contract's constructor's arguments and initialize optionMap
     * @param _applicationId is for tracking the application.
     * @param _article is byte array which acts as a vote subject
     * @param _startTime in second which uses to track the start time of the voting contract
     * @param _deadLine in second which uses to track the end time of the voting contract
     * @param _konaIds which holds konaIds of each participant
     * @param _ballotCount which holds the number of ballots of each participant
     * @dev Change according to requirement
     */
    constructor (
        uint _applicationId,
        uint _votingContractType,
        string _article,
        uint _startTime,
        uint _deadLine,
        bytes32[] _konaIds,
        uint[] _ballotCount,
        address _addressBindingContract
    ) 
        BasicVotingContract (
            _applicationId,
            _votingContractType,
            _article,
            _startTime,
            _deadLine,
            _konaIds,
            _ballotCount,
            _addressBindingContract
        ) 
        public
    {
        
    }
    
    
    /** 
     * @notice This method returns the name of the option according to index.
     * @return _options - list of options selected so far 
     * @dev Change according to requirement
     **/
    function getOption ()
        public
        view
        returns (uint[] _options) 
    {
        return optionList;
    }
    
    
    
    /** 
     * @notice This method returns the total vote count for the option
     * @param _option - option to search for
     * @return _votes - number of votes cast so far for the given option
     * @dev Change according to requirement
     **/
    function getVoteForOption (uint _option)
        public
        view
        returns (uint _votes)
    {
        require(resultMap[_option] != 0, "Invalid Option");
        return resultMap[_option];
    }
    
   
    /** 
     * @notice This method casts vote from specific konaId. This method should be changed.
     * @param _option - which is the array of selections in order of options listed in blockchain
     * @param _konaId which is the id of a participant
     * @dev This method will be changed after reviewing
     **/
    function castVote (
        uint _option,
        bytes32 _konaId
    ) 
        public
        verifyKonaID (_konaId)
        beforeDeadLine
        canCast (_konaId)
       
    {
        updateBallotCount (_konaId);
        if(resultMap[_option] != 0){
            resultMap[_option] += participants[_konaId].ballotCount;
        }
        else{
            resultMap[_option] += participants[_konaId].ballotCount;
            optionList.push(_option);
        }
        
    }
    
     /** 
     * @notice This method returns the total count of options
     * @param _optionCount which is the total count of options
     * @dev Change according to requirement
     **/
     
    function getNumberofOption() public view returns(uint _optionCount){
        return optionList.length;
    }
    
    
}