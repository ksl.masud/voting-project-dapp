pragma solidity ^0.4.22;



/**
 * @title  Contract to register new pair (blockchain address, konaid) for further voting management
 * @notice This contract can be deployed by system admin
 *         New (address, konaid) pair can be registered by Konaserver
 *         Any compromised address can be removed from map
 * @dev    Function definition and calls may be changed upon requirement. Also new funtions can be added.
 */
 
contract AddressBinding{
    
    ///New Address Binding Event will be emitted with user's _blockchainAddress and konaId 
    event NewAddressBindEvent(address indexed _blockchainAddress, bytes32 indexed _konaId);
    ///Any Compromised Address removal event will be emitted with user's _blockchainAddress
    event CompromisedBindingRemovalEvent(address indexed _blockchainAddress);
    
    uint256 private applicationId;                                  ///K-VoB ID
    address private konaServer;                                     ///Kona Server Address
    address private systemAdmin;                                    ///System Admin Address
    mapping (address => bytes32) private  mapAddressToKonaId;       ///bc address to kona id mapping
    
    
    /** 
     * @notice constructor to initialize contract attributes 
     * @param  _applicationId - it indicates the type of application (voting/landmap) 
     * @param  _konaServer - Address of Kona Server to be used for this application
     * @dev    Change according to requirement
     */
     
    constructor(uint256 _applicationId,address _konaServer) public{
        systemAdmin = msg.sender;
        applicationId = _applicationId;
        konaServer = _konaServer;
    }
    
    
     /** 
     * @notice modifier to verify that method call is from Kona Server address 
     * @dev    Change according to requirement
     */
     
    modifier invokedByKonaServer(){
        require(msg.sender == konaServer, "Must be invokedby KonaServer");
        _;
    }
    
    
     /** 
     * @notice modifier to verify that method call is from System Admin Address
     * @dev    Change according to requirement
     */
     
    modifier invokedBySystemAdmin(){
        require(msg.sender == systemAdmin, "Must be invokedby System Admin");
        _;
    }
    
    
    /** 
     * @notice function to map user's _blockchainAddress to given konaid 
     * @param  _blockchainAddress - user's _blockchainAddress
     * @param  _konaId - _konaid for the given _blockchainAddress
     * @dev    Change according to requirement
     */
     
    function bindNewAddress(address _blockchainAddress, bytes32 _konaId) public invokedByKonaServer{
        mapAddressToKonaId[_blockchainAddress] = _konaId;
        emit NewAddressBindEvent(_blockchainAddress, _konaId);
    } 
    
    
    /** 
     * @notice function to get konaId assigned to the caller 
     * @return _konaid - konaid of the caller to the method
     * @dev    Change according to requirement
     */
     
    function queryKonaId() public view  returns(bytes32 _konaid){
        return mapAddressToKonaId[msg.sender];
    }
    
    
     /** 
     * @notice function to remove a compromised _blockchainAddress from the map
     * @dev    Change according to requirement
     */
     
    function removeCompromisedBinding(address _blockchainAddress) 
                                    public 
                                    invokedBySystemAdmin{
       
        delete mapAddressToKonaId[_blockchainAddress];
        emit CompromisedBindingRemovalEvent(_blockchainAddress);
    } 
    
    
     /** 
     * @notice function to retrieve application id
     * @return _applicationId - application id determining application type(voting/landmap)
     * @dev    Change according to requirement
     */
    
    function getApplicationId() public view returns(uint256 _applicationId){
        return applicationId;
    }
    
    
     /** 
     * @notice function to find existence of a pair (_blockchainAddress,_konaid)
     * @param  _blockchainAddress - key of the finding pair
     * @param _konaid - value of the finding pair
     * @return _found - existence status
     * @dev    Change according to requirement
     */
     
    function verifyVoter(address _blockchainAddress, bytes32 _konaid) public view returns(bool _found){
        return (keccak256(mapAddressToKonaId[_blockchainAddress]) == keccak256(_konaid));
        
    }
    
}