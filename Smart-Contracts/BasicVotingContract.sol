pragma solidity ^0.4.22;
import "./AddressBinding.sol";

contract BasicVotingContract {
    
    uint applicationId;                                 // Application id is for indentifing the Application
    string article;                                      // Store arbitary length of article in byte array
    uint startTime;                                     // Start time(second) records the initial time for the voting contract 
    uint deadLine;                                      // Deadlime time(second) records the ending time for the voting contract
    
    struct Participant {
        uint ballotCount;                               // Total ballot for a participant
        bool isCasted;                                  // Weigth for every vote of the participant
    }
    
    uint totalCastedVotes;                              // Total casted vote for this article
    uint totalBallots;                                  // Total vote count for this article
    mapping (bytes32 => Participant) participants;         // Mapping to KonaID to Participant
    address addressBindingContract;                         // Contains AddressBinding contract AddressBinding
    uint votingContractType;                               //Type of the voting contract, 
                                                        // Single = 1 , Multiple = 2, Ranked = 3, Scored =4
    
    /** 
     * @notice verifyKonaID verifies the konaId from AddressBinding contract
     * @param _konaId is used to indentify the participant
     * @dev Change according to requirement
     */
    modifier verifyKonaID (bytes32 _konaId) {
        require (AddressBinding (addressBindingContract).verifyVoter (msg.sender, _konaId), "Caller's kona id is invalid!") ;
        _;
    }
    
    /** 
     * @notice canCast checks the ability of a participant for vote casting
     * @param _konaId is used to indentify the participant
     * @dev Change according to requirement
     */
    modifier canCast (bytes32 _konaId) {
        require (participants[_konaId].ballotCount != 0, "This user has no permission to cast vote!") ;
        require (!participants[_konaId].isCasted, "Already vote has been casted by this participant!") ;
        _;
    }
    
    
     /** 
     * @notice beforeDeadLine the time boundary for this voting contract
     * @dev Change according to requirement
     */
    modifier beforeDeadLine () {
        require (now < deadLine, "Deadline over!") ;
        _;
    }
    
    
    /** 
     * @notice Inserts all necessary values and creats all the participants for tracking vote count.
     * @param _applicationId is for tracking the application.
     * @param _article is byte array which acts as a vote subject
     * @param _startTime in second which uses to track the start time of the voting contract
     * @param _deadLine in second which uses to track the end time of the voting contract
     * @param _konaIds which holds konaIds of each participant
     * @param _ballotCount which holds the number of ballots of each participant
     * @dev Change according to requirement
     */
    constructor (
        uint _applicationId,
        uint _votingContractType,
        string _article,
        uint _startTime,
        uint _deadLine,
        bytes32[] _konaIds,
        uint[] _ballotCount,
        address _addressBindingContract
    )
        internal
    {
        addressBindingContract = _addressBindingContract;
        applicationId = _applicationId;
        votingContractType = _votingContractType;
        article = _article;
        startTime = _startTime;
        deadLine = _deadLine;
        
        //TODO should be checked the length of _konaId and _ballotCount because lengths should be same
        
        for (uint i = 0; i < _konaIds.length; i++) {
            
            totalBallots = totalBallots + _ballotCount[i];
            Participant memory participant = Participant (_ballotCount[i], false);
            participants[_konaIds[i]] = participant;
            
        }
    }
    
    
    
     /** 
     * @notice This method is used to get remaining ballots to cast
     * @dev Change according to requirement
     **/
    function getAvailableBallotsToCast () 
        public
        view
        returns (uint _availableBallotsToCast)
    {
        return totalBallots - totalCastedVotes;
    }
    
    
    
    /** 
     * @notice This method is used to get total ballots of the voting contract
     * @dev Change according to requirement
     **/
    function getBallotCount() 
        public 
        view
        returns (uint _totalBallots) 
    {
        return totalBallots;
    }
    
    
    
    /** 
     * @notice This method is used to checks whether or not time exceeds the deadlime.
     * @dev Change according to requirement
     **/
    function isDeadLinePassed () 
        public 
        view 
        returns (bool) 
    {
        return now > deadLine; 
    }
    
    
    
    /** 
     * @notice This method is used to update ballot count
     * @dev Change according to requirement
     **/
    function updateBallotCount (bytes32 _konaId) 
        internal
        
    {
        totalCastedVotes = totalCastedVotes + participants[_konaId].ballotCount;
        participants[_konaId].isCasted = true;
        
    }
    
    
     /** 
     * @notice This method returns the article of the contract
     * @param _article which is the article of the corresponding voting contract
     * @dev Change according to requirement
     **/
     
    function getArticle() public view returns (string _article){
        return article;
    }
    
    
      /** 
     * @notice This method returns the voting contract type of the contract
     * @param _votingContractType which is an integer indicating  contract type of the corresponding voting contract
     * @dev Change according to requirement
     **/
    
    function getVotingContractType() public view returns(uint _votingContractType){
        return votingContractType;
    }
    
    
    
    
    
}