pragma solidity ^0.4.22;

/**
 * @title  This Contract to register a voting contract which is deployed in blockchain
 * @notice This contract can be deployed by system admin
 *         New (voting Contract address, true) pair can be registered by system admin
 *         Whether any voting contract is registered can be checked
 * @dev    Function definition and calls may be changed upon requirement. Also new funtions can be added.
 */
 
contract VotingRegistry{
    
    ///This event will be emitted at the time of registering a new voting contract
    event NewVotingContractRegistrationEvent(address indexed _votingContractAddress);
    
    address private systemAdmin;                                ///System Admin address
    mapping (address => bool) private mapContractAddressToBool; ///Mapping Voting Contract Address to Boolean  
    
     /** 
     * @notice constructor to initialize system admin address
     * @dev    Change according to requirement
     */
     
    constructor() public {
        systemAdmin = msg.sender;
    }
    
    
     /** 
     * @notice modifier to verify that method call is from System Admin Address
     * @dev    Change according to requirement
     */
     
    modifier invokedBySystemAdmin(){
        require(msg.sender == systemAdmin, "Must be invokedby System Admin");
        _;
    }
    
    
     /** 
     * @notice function to register/map given voting contract   
     * @param  _votingContractAddress - address of a  voting contract
     * @dev    Change according to requirement
     */
     
    function registerVotingContract(address _votingContractAddress) public invokedBySystemAdmin{
        mapContractAddressToBool[_votingContractAddress] = true;
        emit NewVotingContractRegistrationEvent(_votingContractAddress);
    }
    
    
     /** 
     * @notice function to find whether given contract is registered
     * @param  _votingContractAddress - address of voting contract to search in 
     * @return _found - flag
     * @dev    Change according to requirement
     */
     
    function isRegistered(address _votingContractAddress) public view returns(bool _found){
        return mapContractAddressToBool[_votingContractAddress];
    }
    
    
    
}