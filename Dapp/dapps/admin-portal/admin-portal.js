
////necessary variable declaration and initialization

var files;														// contains the kyc file to be uploaded
var _applicationId = 1 ;										// application id 
var konaIdEmail = [];											// konaId list of the voter sent from the server
var konaIds = [];												// converted 32 byte konaId list to be used to publish contract in the blockchain
var weights = [];												// weight list for the voters
var contractAddress;											// the contract address of the published contract in the blockchain
var serverAddress = "https://10.88.233.118:8443";               // server address
var contractDeploymentHtml = '<div id="contract-deployment">'+	// dynamic html element to be added
'<h2>Configure voting pool</h2>'+
'Enter Article: <input type="text" id="article-des"><br>'+
'<select id="drop-down">'+
'<option value="1">Single Select Voting</option>'+
'<option value="2">Multiple Select Voting</option>'+
'<option value="3">Ranked Voting</option>'+
'<option value="4">Score Voting</option>'+
'</select>'+
''+
''+
'</div>';


/**
  * This function will be called on load of the admin portal webpage 
  */

$(document).ready(function () {
	console.log("Admin portal");
	document.getElementById('file').addEventListener('change', handleFileSelect, false);
	////////////
	// document.getElementById("kyc-upload").remove();
	// var y = document.getElementById("contract-deployment");
	// y.style.visibility = "visible";
	// contractDeployment();
});



/**
  * This function handles file select handling at the time of kyc file upload to the server 
  * @param event: event will be generated when file browsing diaglog is opened
  */

function handleFileSelect(event) {
    files = event.target.files; 		// FileList object
    console.log("size "+files[0].size);
    console.log("name "+files[0].name);
}


/**
  * This function will send the kyc file to the server and waits for the response which is a list of kona ids of voters
  */

function getKonaID() {


	var file = files[0];
	var resquestData = new FormData();
	resquestData.append("file",file);

	var request = new XMLHttpRequest();
	request.open("POST", serverAddress+"/register/kycdata", true);


	// request.setRequestHeader("Access-Control-Allow-Origin", "localhost");
	// request.setRequestHeader(header, value)
	request.send(resquestData);
	request.onload = function () {
		if(request.status == 200) {
			responseObject = JSON.parse(request.responseText);
			console.log(responseObject.data);
			var data = responseObject.data;
			for(i=0; i< data.length; i++){
				console.log(data[i].konaId);
				konaIdEmail.push(data[i].konaId);
				konaIds.push("0x" +data[i].konaId);
				weights.push(data[i].weight);
			}
			console.log(konaIds);
			console.log(weights);
			document.getElementById("kyc-upload").remove();				// removing the kyc upload container
			var y = document.getElementById("contract-deployment");		
			y.style.visibility = "visible";								// making the voting configuration container visible
			contractDeployment();										// populating the necessary html elements dynamically 
																		// according to the  choice of  voting methods by the publisher
		} else {
			console.log("server is down!");
		}
	}
	// console.log("HIFI");
}	


/**
  * This function makes related change to the instruction container
  * This function populates the html elements dynamically according to the choice of voting methods by the publisher
  */

function contractDeployment() {
	//var article = document.getElementById("article-des").value;
	document.getElementById("step2").style.background = "repeating-linear-gradient(90deg,white -10000px,green  2000px)";
	document.getElementById("step2").style.color = "white";
	optionGroupToggle();
}


/**
  * This function populates the html elements dynamically according to the choice of voting methods by the publisher
  */

function optionGroupToggle(){
	var optionGroup = document.createElement('div');
	optionGroup.className = "option-group";
	optionGroup.id = "option-list";

	////adding percentile to ranked voting, if selected
	var dropDownElement = document.getElementById("drop-down");
	var selectedMethod = dropDownElement.options[dropDownElement.selectedIndex].value;

	if(selectedMethod==3){							// if ranked voting is selected
		var percentileLabel = document.createElement('label');
		percentileLabel.id = "percentile-label";
		percentileLabel.innerHTML = "Percentile Score";
		optionGroup.appendChild(percentileLabel);

		var percentileTextArea = document.createElement('input');
		percentileTextArea.id = "percentile-des";
		percentileTextArea.placeholder= "Enter Percentile Score";
		optionGroup.appendChild(percentileTextArea);
	}

	////Adding buttons and 2 options
	var btnAdd = document.createElement('BUTTON');
	var txtAdd = document.createTextNode("Add Option");
	btnAdd.id = "add-option";
	btnAdd.appendChild(txtAdd);
	btnAdd.onclick = addOption;

	var btnRemove = document.createElement('BUTTON');
	var txtRemove = document.createTextNode("Remove Option");
	btnRemove.id = "remove-option";
	btnRemove.appendChild(txtRemove);
	btnRemove.onclick = removeOption;

	
	optionGroup.appendChild(btnAdd);
	optionGroup.appendChild(btnRemove);

	var option1 = document.createElement("input");
	option1.type = "text";
	option1.name =  "";
	option1.className = "option-des";
	option1.id = "id1";
	option1.placeholder = "option 1";

	optionGroup.appendChild(option1);

	var option2 = document.createElement("input");
	option2.type = "text";
	option2.name =  "";
	option2.className = "option-des";
	option2.id = "id2";
	option2.placeholder = "option 2";

	optionGroup.appendChild(option2);

    
    var startLabel = document.getElementById("startDate-label");
	var contract_deployment = document.getElementById("contract-deployment");
	contract_deployment.insertBefore(optionGroup,startLabel);
    // dropDownElement.onchange = changeOption;
}


/**
  * This function will be called when the publisher changes the voting methods drop down box
  * This function will call the necessary methods to reveal the appropriate change in the webpage
  */

function changeOption(){
	var dropDownElement = document.getElementById("drop-down");
	var selectedMethod = dropDownElement.options[dropDownElement.selectedIndex].value;
	console.log("Selected item "+selectedMethod);
	if(selectedMethod == 4){		// if score voting is selected
		document.getElementById("option-list").remove();
	}
	else{
		if(document.getElementById("option-list") != undefined)
			document.getElementById("option-list").remove();
	
		optionGroupToggle();
		
	}
}


/**
  * This function will be called when the publisher adds option to  the voting methods (minimum two options must be there except for score voting)
  * This function will call the necessary methods to reveal the appropriate change in the webpage
  */

function addOption(){
	var optionGroup = document.getElementById('option-list'); 
	var option = document.createElement("input");
	option.type = "text";
	option.name =  "";
	option.className = "option-des";
	var dropDownElement = document.getElementById("drop-down");
	var selectedMethod = dropDownElement.options[dropDownElement.selectedIndex].value;
	//ranked
	if(selectedMethod == 3){ 
		option.id = "id"+((optionGroup.childElementCount - 4) + 1);
		option.placeholder = "option "+((optionGroup.childElementCount - 4) + 1);
	}
	// single/mulitiple select
	else{	
		option.id = "id"+((optionGroup.childElementCount - 2) + 1);
		option.placeholder = "option "+((optionGroup.childElementCount - 2) + 1);
	}
	optionGroup.appendChild(option);
}


/**
  * This function will be called when the publisher removes option from  the voting methods (minimum two options must be there except for score voting)
  * This function will call the necessary methods to reveal the appropriate change in the webpage
  */

function removeOption(){
	//
	var dropDownElement = document.getElementById("drop-down");
	var selectedMethod = dropDownElement.options[dropDownElement.selectedIndex].value;

	if((selectedMethod == 3  && document.getElementById("option-list").childElementCount > 6) || 
						(selectedMethod!=3 && document.getElementById("option-list").childElementCount > 4))
	{
		var x = document.getElementById("option-list");
		if (x.lastChild != undefined) {
			x.removeChild(x.lastChild);
		}
	}
	
}


/**
  * This function will be called when the publisher completes the configuration input and wants to upload the contract to the blockchain for mining
  */

function launchVote(){
	var article = document.getElementById("article-des").value;
	var votingType = document.getElementById("drop-down").selectedIndex + 1;

	console.log("article");
	console.log(article);
	console.log("votingType");
	console.log(votingType);
	
	
	// gathering the values from the html elements
	var startTime = document.getElementById("start-date-picker").value;
	var endTime = document.getElementById("end-date-picker").value;

	console.log("startTime");
	//spliting time into month , date and year
	var startTimeComponents = startTime.split("-"); 
	console.log(startTimeComponents);
	//converting input time to epoch format
	var startEpoch = Date.UTC(startTimeComponents[0], startTimeComponents[1], 
		startTimeComponents[2]);
	console.log(startEpoch);

	console.log("endTime");
	//spliting time into month , date and year
	var endTimeComponents = endTime.split("-"); 
	console.log(endTimeComponents);
	//converting input time to epoch format
	var endEpoch = Date.UTC(endTimeComponents[0], endTimeComponents[1], 
		endTimeComponents[2]);
	console.log(endEpoch);
	
	if(startEpoch >= endEpoch){
		alert("Invalid Voting Duration");
	}

	var options = [];
	if(votingType!=4){ //checking whether score voting is selected
		var optionGroup = document.getElementById("option-list");
		if(selectedMethod == 3){ 
			for(i=1; i<=optionGroup.childElementCount-4;i++){
				options.push(web3.fromAscii(document.getElementById("id"+i).value));
			}
		}
		// single/mulitiple select
		else{	
			for(i=1; i<=optionGroup.childElementCount-2;i++){
				options.push(web3.fromAscii(document.getElementById("id"+i).value));
			}
		}
		
		console.log("options");
		console.log(options);
	}

	///////////////////////////////////////////////
	
	var _votingContractType = votingType ;
	var _article = article ;
	var _startTime = startEpoch ;
	var _deadLine = endEpoch ;
	var _konaIds = konaIds ;
	var _ballotCount = weights ;
	var _options = options ;
	var _addressBindingContract = "0x9ba0211efd12adc08243e38110f3de3dd3fefb02";  // This is just given as a garbage value for testing
																				 // when address binding webpage is prepared, it will properly integrated

	var dropDownElement = document.getElementById("drop-down");
	//Single Select Voting 
	var selectedMethod = dropDownElement.options[dropDownElement.selectedIndex].value;
	if(selectedMethod==1){
		publishSingleSelect(
							_votingContractType,
							_article,
							_startTime,
							_deadLine,
							_konaIds,
							_ballotCount,
							_options,
							_addressBindingContract
							);
	}
	//Multiple Select Voting 
	else if(selectedMethod==2){
	  publishMutipleSelect( 
							_votingContractType,
							_article,
							_startTime,
							_deadLine,
							_konaIds,
							_ballotCount,
							_options,
							_addressBindingContract
							);
	}
	//Ranked Select Voting 
	else if(selectedMethod==3){
		var _hundredPercentileScore = document.getElementById("percentile-des").value;
		publishRanked(
					   _votingContractType,
					   _article,
					   _startTime,
					   _deadLine,
					   _konaIds,
					   _ballotCount,
					   _options,
					   _hundredPercentileScore,
					   _addressBindingContract
					   );
	}
	//Score Voting 
	else if(selectedMethod==4){
		publishScore(
				   _votingContractType,
				   _article,
				   _startTime,
				   _deadLine,
				   _konaIds,
				   _ballotCount,
				   _addressBindingContract);
	}
}


/**
  * This function submits the contract for single select voting
  */
function publishSingleSelect(
							_votingContractType,
							_article,
							_startTime,
							_deadLine,
							_konaIds,
							_ballotCount,
							_options,
							_addressBindingContract){
	var singleselectvoting = singleselectvotingContract.new(
		_applicationId,
		_votingContractType,
		_article,
		_startTime,
		_deadLine,
		_konaIds,
		_ballotCount,
		_options,
		_addressBindingContract,
		{
			from: web3.eth.accounts[0], 
			data: singleselectvotingContractData,
			gas: '4700000'
		}, function (e, contract){
			console.log(e, contract);
			if (typeof contract.address !== 'undefined') {
				console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
				contractAddress = contract.address;
				renderMailBox();
			}
		}
	);
}


/**
  * This function submits the contract for multiple select voting
  */

function publishMutipleSelect(  
								_votingContractType,
								_article,
								_startTime,
								_deadLine,
								_konaIds,
								_ballotCount,
								_options,
								_addressBindingContract){
	var multipleselectvoting = multipleselectvotingContract.new(
	   _applicationId,
	   _votingContractType,
	   _article,
	   _startTime,
	   _deadLine,
	   _konaIds,
	   _ballotCount,
	   _options,
	   _addressBindingContract,
	   {
	     from: web3.eth.accounts[0], 
	     data: multipleselectvotingContractData,
	     gas: '4700000'
	   }, function (e, contract){
	    console.log(e, contract);
		    if (typeof contract.address !== 'undefined') {
		         console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
		         contractAddress = contract.address;
				 renderMailBox();
		    }
 		}
 	);
}


/**
  * This function submits the contract for ranked voting
  */

function publishRanked(
					   _votingContractType,
					   _article,
					   _startTime,
					   _deadLine,
					   _konaIds,
					   _ballotCount,
					   _options,
					   _hundredPercentileScore,
					   _addressBindingContract){
	var rankedvoting = rankedvotingContract.new(
	   _applicationId,
	   _votingContractType,
	   _article,
	   _startTime,
	   _deadLine,
	   _konaIds,
	   _ballotCount,
	   _options,
	   _hundredPercentileScore,
	   _addressBindingContract,
	   {
	     from: web3.eth.accounts[0], 
	     data: rankedvotingContractData, 
	     gas: '4700000'
	   }, function (e, contract){
	    console.log(e, contract);
		    if (typeof contract.address !== 'undefined') {
		         console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
		         contractAddress = contract.address;
				renderMailBox();
		    }
	 	}
	);
}

/**
  * This function submits the contract for score voting
  */

function publishScore(
					   _votingContractType,
					   _article,
					   _startTime,
					   _deadLine,
					   _konaIds,
					   _ballotCount,
					   _addressBindingContract){
	var scorevoting = scorevotingContract.new(
	   _applicationId,
	   _votingContractType,
	   _article,
	   _startTime,
	   _deadLine,
	   _konaIds,
	   _ballotCount,
	   _addressBindingContract,
	   {
	     from: web3.eth.accounts[0], 
	     data: scorevotingContractData, 
	     gas: '4700000'
	   }, function (e, contract){
	    console.log(e, contract);
		    if (typeof contract.address !== 'undefined') {
		         console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
		         contractAddress = contract.address;			// saving the mined contract's blockchain address
				 renderMailBox();								// initiates the next tast to send mail to the voters
		    }
	 	}
	);
}

/**
  * This function changes the html view for sending mail
  */

function renderMailBox() {
	document.getElementById("contract-deployment").remove();		// removing voting configuration html container
	var y = document.getElementById("mail-sending");
	y.style.visibility = "visible";									// making the mail sending container visible
	document.getElementById("step3").style.background = "repeating-linear-gradient(90deg,white -10000px,green  2000px)";
	document.getElementById("step3").style.color = "white";
}

/**
  * This function sends mail to the voters
  */

function doSendEmail(){

	var votingContractAddress = contractAddress;
	var konaIdList = konaIdEmail;
	SendEmail(votingContractAddress, konaIdList, serverAddress);
}
