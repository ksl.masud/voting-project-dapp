
 
 /**
  * SendEmail sends a request to send e-mail with the voting contract address.
  * @param serverAddress: address of the server. example: http://127.0.0.1:8080
  * @param konaIdList: List of users who can cast vote
  * @param votingContractAddress: Smart contract address to cast vote
  */
 
function SendEmail(votingContractAddress, konaIdList, serverAddress){
	var response;
  $.ajax({
    url : serverAddress + '/api/sendMail',
	async: false,
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({"votingContractAddress": votingContractAddress, "konaIdList": konaIdList}),
    processData: false,
    success: function (data){
      console.log('success ');
	  console.log(data);
	  response = data;
    },
    error: function (data){
      console.log('ERROR: ');
      console.log(data);
	  response = data;
    }
  });
  return response;
} 